#!/usr/bin/env bash

set -e

module load gromacs
module load matplotlib

source analysis_functions.sh

RMSD_THRESHOLD=0.6
SUBSAMPLE_RFRAC=100

outdir=rmsds
ndxtype=C-alpha
natives=native-confs
ndxs=ndx
ndxname=C-alpha.ndx


calc_rmsds() {
    workarea="$1"
    name="$2"
    hid=$3
    wa=$4
    traj="$5"

    dest=$outdir/$name/$wa
    [ ! -d $dest ] && mkdir -p $dest

    g_rms \
	-s $natives/hipin-$hid-folded.pdb \
	-f $traj \
	-n $ndxs/$hid/$ndxname \
	-o $dest/rmsd.xvg

}

rm -rf $outdir

map_trajs calc_rmsds

tmpdir=$(mktemp -d)

for sys in $outdir/hipin-*-*; do
    [ ! -d $sys ] && continue
    name=$(basename $sys)
    dest=$outdir/$name

    hid=$(getnum $(basename $sys))
    typ=$(getConformationType $sys)
    mutation=$(egrep "^$hid " mutation-map.dat | awk '{print $2}')
    datdir=$tmpdir/$typ
    [ ! -d $datdir ] && mkdir $datdir

    ./plotxvgs.py -t "$mutation" -o $dest.png -f $dest/*/*.xvg

    cmd="./plotRmsdByFraction.py -e '$mutation' -T $RMSD_THRESHOLD -o $dest-rfrac.png -i $SUBSAMPLE_RFRAC -x 0.001 -X 'Time ($\mu s$)' -c $datdir/$hid.dat"
    for xvg in $sys/*workarea-*/*.xvg; do
	cmd="$cmd -t $xvg"
    done
    eval "$cmd"

done

./plotRMSDFraction.py $outdir/folded-rmsdfraction.png mutation-map.dat $tmpdir/F/*.dat
./plotRMSDFraction.py $outdir/ext-rmsdfraction.png mutation-map.dat $tmpdir/E/*.dat

rm -rf $tmpdir
tar cvjf $outdir.tar.bz2 $outdir
