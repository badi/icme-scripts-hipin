#!/usr/bin/env bash

module load cctools

gpuid=$1

echo "runworker: running on $1"
nvidia-smi -i $gpuid

export GPUID=$gpuid
work_queue_worker -N icme-hipin -a -t 7d
