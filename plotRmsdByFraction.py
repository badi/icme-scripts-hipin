#!/usr/bin/env python

import xvglib

import numpy as np
import pylab as plt

import argparse, glob, sys, operator


def getopts():
	p = argparse.ArgumentParser()
	p.add_argument('-t', '--trajglobs', nargs='+', action='append', help='Files matching this pattern represent one trajectory')
	p.add_argument('-T', '--rmsd-threshold', default=5, type=float, help='Threshold RMSD in nanometers')
	p.add_argument('-o', '--outfile', default='/tmp/plot.png')
	p.add_argument('-c', '--countsfile', help='Save the total counts to this file if specified')
	p.add_argument('-x', '--xmult', default=1, type=float)
	p.add_argument('-X', '--xlabel', default='Time')
	p.add_argument('-i', '--subsample', default=1, type=int)
	p.add_argument('-e', '--title', default=None)

	return p.parse_args()


class Trajectory(object):
	def __init__(self, time=np.array([]), vals=np.array([])):
		Trajectory._sanity(time, vals)
		self._time = time
		self._vals = vals

	@classmethod
	def _sanity(cls, ts, vs):
		assert ts.shape == vs.shape

	def add_series(self, time, vals):
		Trajectory._sanity(time, vals)
		self._time = np.append(self._time, time)
		self._vals = np.append(self._vals, vals)

	def count_matching(self, val, cmp='<='):
		funcs = {
			'<' : operator.lt,
			'<=': operator.le,
			'>' : operator.gt,
			'>=': operator.ge,
			'!=': operator.ne}
		f = funcs[cmp]
		return len(self._vals[f(self._vals, val)])


	@property
	def time(self):
		return self._time

	@property
	def values(self):
		return self._vals

	@property
	def length(self):
		return len(self._time)

	def __repr__(self):
		return '<Trajectory: %s frames>' % self.length

class Trajlist(object):
	def __init__(self, trajlist=None):
		self._list = trajlist or list()

	def append(self, traj):
		self._list.append(traj)

	def get_values(self, idx):
		vals = []
		for traj in self._list:
			if idx < traj.length:
				vals.append(traj.values[idx])
		return vals

	def count_matching(self, val, cmp='<='):
		count = 0
		for t in self._list:
			c = t.count_matching(val, cmp=cmp)
			count += c
		return count


	@property
	def count(self):
		count = 0
		for t in self._list:
			count += t.length
		return count


def load_trajectories(globs):
	trajectories = Trajlist()
	for gi, group in enumerate(globs):
		traj = Trajectory()
		for ti, pattern in enumerate(group):
			files = glob.glob(pattern)
			for path in files:
				print 'Group', gi, 'traj', ti, 'loading', path
				data = np.array(list(xvglib.load_xvg(path)))
				ts = data.T[0,:]
				vs = data.T[1,:]
				traj.add_series(ts, vs)
		trajectories.append(traj)

	return trajectories


def fractionMatching(threshold, trajectories):
	fraction = list()
	counts = list()
	idx = 0
	while True:
		vals = trajectories.get_values(idx)
		if len(vals) <= 0: break

		matches = map(lambda v: v <= threshold, vals)
		f = len(filter(None, matches)) / float(len(vals))
		fraction.append(f)
		counts.append(len(vals))
		idx += 1
	return np.array(fraction), np.array(counts)

if __name__ == '__main__':
	opts = getopts()

	trajs = load_trajectories(opts.trajglobs)

	if opts.countsfile:
		nmatches = trajs.count_matching(opts.rmsd_threshold, cmp='<=')
		length   = trajs.count
		print 'Saving', opts.countsfile
		with open(opts.countsfile, 'w') as fd:
			fd.write('#matching total\n')
			fd.write('%d %d\n' % (nmatches, length))

	frac, counts = fractionMatching(opts.rmsd_threshold, trajs)
	time = np.arange(len(frac)) * opts.xmult

	ixs = np.arange(0, len(frac), opts.subsample)
	counts = counts[ixs]
	time = time[ixs]
	frac = frac[ixs]

	plt.subplot('211')
	plt.title('%s Fraction RMSD <= %.2f $\AA$' % (opts.title, opts.rmsd_threshold * 10))

	plt.plot(time, counts)
	plt.ylim(ymax=max(counts) + 0.5, ymin=-0.5)
	plt.ylabel('Sample size')

	plt.subplot('212')
	plt.plot(time, frac)
	plt.ylim(ymin=-0.1, ymax=1.1)
	plt.ylabel('Fraction')
	plt.xlabel(opts.xlabel)

	print 'Saving', opts.outfile
	plt.savefig(opts.outfile)

