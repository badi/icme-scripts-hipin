

map_trajs() {
# A) requires trjconv from GMX to be in the PATH
# B) accepts a function of the following type
#    func :: <workarea :: str> <name :: str> <hipin id :: int> <workarea name :: str> <path to traj>

    local func=$1

    local timestep_ps=1000
    local tempdir=$(mktemp -d)

    for sys in hipin-gpu-solvimpl/hipin-*-*; do

	for workarea in $sys/*workarea-*; do

	    local traj=$workarea/traj.xtc
	    [ ! -f $traj ] && continue

	    local name=$(basename $sys)
	    local wa=$(basename $workarea)
	    local hid=$(echo $name | sed 's/[^0-9]//g')

	    local temptraj=$tempdir/$name-$wa.xtc
	    trjconv -f $traj -o $temptraj -timestep $timestep_ps

	    $func $workarea $name $hid $wa $temptraj

	    rm $temptraj

	done &

    done

    wait

    rm -rvf $tempdir

}

getConformationType() {
### Gets the conformation type (extended, folded, unknown) from the name
### (eg hipin-42-ext)
### This assumes that the conformation type is present in the name as *-{type}

    local sysname=$1
    case "$sysname" in
	*-ext)
	    echo "E";; # extended
	*-folded)
	    echo "F";; # folded
	*)
	    echo "U";; # unknown
    esac
}

getnum() {
### Strips all non-numerical values from the string
    echo $1 | sed 's/[^0-9]//g'
}


getJobNameBase() {
### Get the job name as listed in 'qstat'
### Paramters:
###   <hid :: int : the mutant id>
###   <wid :: int : the workarea id>
###   <typ :: str : the output of a call to 'getConformationType'>

    local hid=$1
    local wid=$2
    local typ=$3

    echo h${hid}${typ}.${wid}
}

getJobName() {
### Calls down to 'getJobNameBase'
### Parameters:
###   <system :: str : in the form of "foo/bar/baz-[0-9]+-bang">
###   <workarea :: str : in the form of "foo-[0-9]+">
### The mutant id must be present as the numerical value of the basename of 'system'

    local system=$(basename $1)
    local workarea=$(basename $2)

    local hid=$(getnum $system)
    local wid=$(getnum $workarea)
    local typ=$(getConformationType $system)

    getJobNameBase $hid $wid $typ
}


getJobId() {
### Get the job id from the job name
### Parameters:
###   <name :: str : job name as reported by qstat>
### Returns:
###   <int :: the moab job id or empty string>
    local name=$1
    qstat -u izaguirr | grep "$name" | awk '{print $1}' | sed 's/[^0-9]//g'
}

isJobnameRunning() {
### Check if the job is running.
### Parameters:
###   <name :: str : the job name as reported by qstat>
### Returns:
###   "" (empty string) for false
###   true otherwise
    local name=$1
    local jid=$(getJobId $name)
    [ -z $jid ] && echo "" || echo true
}

killJobNames() {
### Cancels the jobs if they are running
### Parameters:
###   <names :: n >= 1 : The job names (as reported by qstat) to kill>
    local names="$@"
    for jname in $names; do
	local running=$(isJobnameRunning $jname)
	[ ! $running ] && continue
	local jid=$(getJobId $jname)
	echo $jid
    done | xargs canceljob
}