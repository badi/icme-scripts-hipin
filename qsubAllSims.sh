#!/usr/bin/env bash

WORKAREA_PAT=workarea-*

source analysis_functions.sh


for workarea in $(find hipin-gpu-solvimpl -name "$WORKAREA_PAT"); do

    sys=$(dirname $workarea)

    [ ! -d $workarea ] && continue

    jname=$(getJobName $sys $workarea)

    # don't submit if the job is already in the queue
    running=$(qstat -u izaguirr | grep '^[0-9]*\.icme-gpu\.cm' | awk '{print $4}' | grep "$jname")
    [ -z "$running" ] || continue

    echo $jname $workarea
    ./qsubsim.sh $jname $workarea
    sleep 1

done
