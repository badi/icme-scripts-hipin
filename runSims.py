import work_queue as wq

import os, glob, string

wq.set_debug_flag('wq')


PREFIX = 'hipin-gpu-solvimpl'
RUNSIM = 'runsim.sh'


def get_workareas():
    WORKAREA_PAT = 'workarea-*'

    for s in ['folded', 'ext']:
        hipin = 'hipin-*-' + s
        pattern = os.path.join(PREFIX, hipin, WORKAREA_PAT)
        for path in glob.iglob(pattern):
            yield path

def get_int(s):
    n = filter(lambda c: c in string.digits, s)
    return int(n)

def get_type(path):
    if path.endswith('-ext'):
        return 'E'
    elif path.endswith('-folded'):
        return 'F'
    else:
        return 'U'

def get_tag(workarea):
    wid = get_int (os.path.basename(workarea))
    hid = get_int (os.path.basename(os.path.dirname(workarea)))
    typ = get_type(os.path.basename(os.path.dirname(workarea)))
    return 'h%d%s.%d' % (hid, typ, wid)
    


q = wq.WorkQueue(port=wq.WORK_QUEUE_RANDOM_PORT, name='icme-hipin', catalog=True)

running_tags  = set()

while True:

    for wa in get_workareas():

        runsim = os.path.abspath(RUNSIM)
        wa     = os.path.abspath(wa)
        tag    = get_tag(wa)

        if tag not in running_tags:

            cmd = '%s %s' % (runsim, wa)
            t = wq.Task(cmd)
            t.specify_tag(tag)

            print 'Executing:', t.command
            running_tags.add(tag)
            q.submit(t)



    t = q.wait(10)
    if t:
        print t.output
        running_tags.discard(t.tag)
