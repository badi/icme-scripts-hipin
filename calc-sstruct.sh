#!/usr/bin/env bash

set -e

module load gromacs
module load imagemagick

source analysis_functions.sh

ndxs=ndx
ndxname=System.ndx
outdir=sstruct


calc_sstruct() {
    workarea="$1"
    name="$2"
    hid=$3
    wa=$4
    traj="$5"

    dest=$outdir/$name/$wa
    [ ! -d $dest ] && mkdir -p $dest

    do_dssp \
	-f 	$traj		\
	-s 	$workarea/topol.tpr 	\
	-n 	$ndxs/$hid/$ndxname 	\
	-ssdump $dest/ssdump.dat 	\
	-o 	$dest/ss.xpm 		\
	-sc 	$dest/scount.xvg 	\
	-a 	$dest/area.xpm 		\
	-ta 	$dest/totarea.xvg 	\
	-aa 	$dest/averarea.xvg

    sed -i "s/Secondary structure/$name $wa/" $dest/ss.xpm

    xpm2ps \
	-f $dest/ss.xpm 	\
	-o $dest/ss.eps 	\
	-by 10 		\
	-size 1000

    convert $dest/ss.{eps,png}
}


rm -rf $outdir

map_trajs calc_sstruct

rm -v \#dd*
tar cvjf $outdir.tar.bz2 $outdir/*/*/*.png
