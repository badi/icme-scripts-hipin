#!/usr/bin/env bash

HIPIN_PAT=hipin-*-*
WORKAREA_PAT=workarea-*
DIVVAL=500

source analysis_functions.sh

PRINTF_PAT='%-9s %15s\t'

printf "$PRINTF_PAT" "Job Name" "Length (ns)"
printf "\n"
echo "--------------------------------"

for sys in hipin-gpu-solvimpl/$HIPIN_PAT; do
    for workarea in $sys/$WORKAREA_PAT; do

	[ ! -d $workarea ] && continue

	jname=$(getJobName $sys $workarea)
	[ ! $(isJobnameRunning $jname) ] && continue
	traj=$workarea/traj.xtc

	nframes=$(gmxcheck -f $traj 2>&1 | egrep '^Step *[0-9]*' | awk '{print $2}')
	nhashes=$(echo "$nframes / $DIVVAL" | bc)

	printf "$PRINTF_PAT" $jname $nframes
	while true; do
	    [ $nhashes -le 0 ] && break
	    printf '#'
	    nhashes=$(( $nhashes - 1 ))
	done

	printf "\n"

    done
done  | sort -n -k 2
