#!/usr/bin/env python

import xvglib

import numpy as np
import pylab as plt

import glob
import itertools
import os
import re
import sys
import argparse

def getopts():
	p = argparse.ArgumentParser()
	p.add_argument('-x', '--xlabel', default='Time ($\mu$s)')
	p.add_argument('-y', '--ylabel', default='RMSD ($\AA$)')
	p.add_argument('-t', '--title',  default='')
	p.add_argument('-X', '--maxX',   default=None, type=float)
	p.add_argument('-Y', '--maxY',   default=None, type=float)
	p.add_argument('-o', '--outfile')
	p.add_argument('-f', '--xvgs', nargs='+')
	p.add_argument('-T', '--tfactor', default=0.000001, type=float, help='Multiply the time values by this')
	p.add_argument('-R', '--rfactor', default=10, type=float, help='Multiply the rmsd values by this')

	return p.parse_args()

def load_xvg(path, tmult=1., rmult=1.):
	results = []
	for t, r in xvglib.load_xvg(path):
				t *= tmult
				r *= rmult
				results.append((t,r))
	return np.array(results)




def plot_paths(paths, outfile='/tmp/plot.png', tmult=1., rmult=1.,
			   xlabel='', ylabel='', title='',
			   maxX=None, maxY=None
			   ):

	def label(path):
		from os.path import dirname, basename
		return basename(dirname(path))

	for path in paths:
		data = load_xvg(path, tmult=tmult, rmult=rmult)
		ts = data.T[0,:]
		rs = data.T[1,:]
		plt.plot(ts, rs, alpha=0.75, label=label(path))

	plt.xlabel(xlabel)
	plt.ylabel(ylabel)
	plt.title(title)
	plt.grid()
	plt.legend(loc='upper right')

	plt.xlim(xmin=0)
	plt.ylim(ymin=0)

	if maxX: plt.xlim(xmax=maxX)
	if maxY: plt.ylim(ymax=maxY)

	plt.savefig(outfile)
	plt.close()
	print 'Saved', outfile
	

if __name__ == '__main__':
	opts = getopts()
	plot_paths(opts.xvgs, outfile=opts.outfile, tmult=opts.tfactor, rmult=opts.rfactor, xlabel=opts.xlabel, ylabel=opts.ylabel, title=opts.title, maxX=opts.maxX, maxY=opts.maxY)
