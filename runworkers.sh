#!/usr/bin/env bash

module load cctools

cd /home/izaguirr/cabdulwa/hipin

nvidia-smi

while true; do
    source chooseGPU.sh
    [ -z $GPUID ] && break
    echo "runworkers: Chose GPUID=$GPUID"
    ./runworker.sh $GPUID &
    sleep 60
done

echo "runworkers: waiting for workers..."
wait
