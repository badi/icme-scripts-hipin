#!/usr/bin/env bash

set -e

jname=$1
workarea="$2"

qsub \
    -N $jname \
    -l 'nodes=1:ppn=1:gpus=1' \
    -l 'walltime=24:00:00' \
    -V \
    -q longq \
    <<EOF
$(readlink -f runsim.sh) $(readlink -f $workarea)
EOF

echo $jname $workarea
