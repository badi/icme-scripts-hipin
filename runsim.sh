#!/usr/bin/env bash

workarea=$(readlink -f $1)

#source ~/cabdulwa/hipin/chooseGPU.sh
gpuid=$GPUID

echo "Got GPUID=$gpuid"

module load gromacs/openmm

echo "Workarea: $workarea"

[ ! -d $workarea ] && mkdir -vp $workarea
cd $workarea

tpr=topol.tpr
if [ ! -f $tpr ]; then
    seed=$RANDOM
    sed "s/\(.*seed *=\).*$/\1 $seed/" ../sim.mdp >sim.mdp
    grompp \
	-f sim.mdp \
	-c ../conf.gro \
	-p ../topol.top \
	-o $tpr
fi

now=$(date "+%F_%T")


mdrun-gpu \
    -s $tpr \
    -cpi state.cpt \
    -cpo state.cpt \
    -append \
    -maxh 4 \
    -device "OpenMM:deviceid=$gpuid" \
    1>stdout.$now.log \
    2>stderr.$now.log
