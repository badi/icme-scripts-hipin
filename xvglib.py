

import re, itertools

def load_xvg(path):
	"""
	Return an iterator over the x, y values in an xvg file.
	"""

	regex = re.compile(r'(?P<time>\d+\.\d+)\s+(?P<value>\d+\.\d+)')
	with open(path) as fd:
		for line in itertools.imap(str.strip, fd):
			m = regex.match(line)
			if m:
				t = float(m.group('time'))
				v = float(m.group('value'))
				yield t, v
