#!/usr/bin/env bash

START=0
GPUS=$(echo $(( $(nvidia-smi  -L | wc -l) - 1 )))
unset GPUID

echo "chooseGPU: START=$START"


for gid in $(seq $START $GPUS); do
    echo "chooseGPU: Trying gpu $gid"
    usage=$(nvidia-smi -i $gid | grep -i 'No running compute processes found')
    if [[ ! -z "$usage" ]]; then
	export GPUID=$gid
	echo "chooseGPU: Set GPUID=$GPUID"
	break
    fi
done

if [[ -z $GPUID ]]; then
    echo "chooseGPU: Could not find an empty GPU"
    unset GPUID
fi
