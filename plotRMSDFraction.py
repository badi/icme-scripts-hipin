#!/usr/bin/env python

import numpy as np
import pylab as plt

import os, sys, itertools


def loadmap(path):
	mapping = {}
	with open(path) as fd:
		for line in itertools.imap(str.strip, fd):
			if not line: continue
			k, v = line.split()
			mapping[k] = v
	return mapping

if __name__ == '__main__':
	outfile   = sys.argv[1]
	mapfile   = sys.argv[2]
	datafiles = sys.argv[3:]

	npoints = len(datafiles)
	ind     = np.arange(npoints)
	width = 1

	mapping = loadmap(mapfile)

	names = []
	for i, path in enumerate(datafiles):
		name, _ = os.path.splitext(os.path.basename(path))
		names.append(mapping[name])
		nmatch, total = np.loadtxt(path, unpack=True)
		plt.bar(i, nmatch / float(total), width)

	plt.xticks(ind + width / 2., names, rotation=30)
	plt.ylim(ymin=-0.1, ymax=1.1)

	plt.xlabel('Mutation')
	plt.ylabel('Fraction')
	plt.savefig(outfile, bbox_inches='tight', pad_inches=0.5)
