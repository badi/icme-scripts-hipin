#!/usr/bin/env bash

[ -z $1 ] && njobs=1 || njobs=$1

source analysis_functions.sh

echo "Finding shortest simulations"
names=$(./showLengths.sh | tail -n $njobs | awk '{print $1}')
echo "Found:"
cat <<EOF
$names
EOF

for jname in $names; do
    jid=$(qstat -u izaguirr | grep "$jname" | awk '{print $1}' | sed 's/[^0-9]//g')
    [ -z $jid ] && continue
    echo $jid
done | xargs canceljob
